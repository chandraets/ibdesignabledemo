//
//  CartView.swift
//  IBDesignableDemo
//
//  Created by Puja Bhagat on 30/11/2021.
//

import UIKit

 @IBDesignable class CartView: UIView {
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    //Initialize class from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    //Initialize class from storyboard
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override func prepareForInterfaceBuilder() {
        lblTitle.text = "Hello World!"
        lblDesc.text = "IBDesignable class"
    }
    
    private func commonInit() {
        view = loadViewFromNib(nibName: "CartView")
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.frame = self.bounds
        addSubview(view)
    }
    
    func loadViewFromNib(nibName: String) -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
